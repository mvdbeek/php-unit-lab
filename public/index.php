<?php

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, array $args): Response {
    $m = new Mustache_Engine();
    $response->getBody()->write($m->render('Hello, {{planet}}!', array('planet' => 'World')));
    return $response;
});

$app->run();